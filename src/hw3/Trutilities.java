package hw3;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Trutilities {
//	public static void main(String[] args) {
		// System.out.println(Trutilities.isOrdered(5, 4, 3, 2, 1));
		// System.out.println(Trutilities.isGreenish("#fffeff"));
		// System.out.println(Trutilities.isMilitary("2499"));
		// Trutilities.isImage(new File("bowie.docx"));
		// System.out.println(Trutilities.hasMultipleDots(".."));
		// System.out.println(Trutilities.fitsAspect(740, 320, 2.0));
		// System.out.println(Trutilities.fitsWithin(5, 5, 4, 3));
		// System.out.println(Trutilities.isIllegal("nor"));
//		System.out.println(Trutilities.isFaster("4:45", "4:46"));
//		System.out.pringln(Trutilities.isOld(file, year, month, date))
//	}

	public static boolean isOrdered(int a, int b, int c, int d, int e) {
		int arr[] = { a, b, c, d, e };
		return (a >= b && b >= c && c >= d && d >= e) || (a <= b && b <= c && c <= d && d <= e);

	}

	public static boolean isGreenish(String color) {
		int red = Integer.parseInt(color.substring(1, 3), 16);
		int green = Integer.parseInt(color.substring(3, 5), 16);
		int blue = Integer.parseInt(color.substring(5, 7), 16);

		color = String.format("#%02x%02x%02x", red, green, blue);
		return green > red && green > blue;
	}

	public static boolean isMilitary(String militaryTime) {
		int hour = Integer.valueOf(militaryTime.substring(0, 2));
		int minute = Integer.valueOf(militaryTime.substring(2, 4));

		return hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59;

	}

	public static boolean isImage(java.io.File file) {
		String name = file.getName();
		int tmpNameFrom = name.lastIndexOf(".");
		String tmpName = name.substring(tmpNameFrom + 1);

		return tmpName.equalsIgnoreCase("png") || tmpName.equalsIgnoreCase("gif") || tmpName.equalsIgnoreCase("jpg") || tmpName.equalsIgnoreCase("jpeg");
		
	}

	public static boolean hasMultipleDots(String text) {
		int length1 = text.length();
		String newText = text.replaceAll("\\.", "");
		int length2 = newText.length();
		return (length1 - length2) >= 2;

	}

	public static boolean fitsAspect(int width, int hight, double aspectRatio) {
		double a = Math.abs(aspectRatio - (float) width / (float) hight);
		return a <= 0.001;
	}

	public static boolean fitsWithin(int widthA, int hightA, int widthB, int hightB) {
		return (widthA <= widthB && hightA <= hightB) || (widthA <= hightB && hightA <= widthB);
	}

	public static boolean isFaster(String A, String B) {
		
		int a = A.indexOf(":");
		
		int hourA = Integer.valueOf(A.substring(0,a));
		Integer minuteA = Integer.valueOf(A.substring(a+1));
		Integer timeA = hourA * 60 + minuteA;
		
		int b = B.indexOf(":");
		int hourB = Integer.valueOf(B.substring(0, b));
		int minuteB = Integer.valueOf(B.substring(b+1));
		int timeB = hourB * 60 + minuteB;
	
		return timeA < timeB;
		
			
	}

	public static boolean isIllegal(String direction) {

		if (direction.equalsIgnoreCase("North") || direction.equalsIgnoreCase("South")
				|| direction.equalsIgnoreCase("East") || direction.equalsIgnoreCase("West")) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean isOld(java.io.File file, int year, int month, int date) {
		long mod = file.lastModified();		 
		GregorianCalendar gc = new GregorianCalendar(year, month - 1, date);
        
		return gc.getTimeInMillis() >= mod;
		
		

	}
	
}
