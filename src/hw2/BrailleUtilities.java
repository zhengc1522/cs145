package hw2;

import java.util.Scanner;

public class BrailleUtilities {
	public static final String RAISED = "\u2022";
	public static final String UNRAISED = "\u00b7";
	public static final String LETTER_SPACER = "  ";
	public static final String WORD_SPACER = "    ";
	
	public static void main(String[] args){
		
	}
	
	public static String translateTopLine(String str){
		
		str = str.toLowerCase();
		String newStr = str.replaceAll(" ", WORD_SPACER);
		newStr = newStr.replaceAll("[cdfgmnpqxy]",RAISED+RAISED+LETTER_SPACER);
		newStr = newStr.replaceAll("[abehkloruvz]",RAISED+UNRAISED+LETTER_SPACER);
		newStr = newStr.replaceAll("[ijstw]",UNRAISED+RAISED+LETTER_SPACER);
		newStr = newStr.trim();
		return newStr;
	}
	
	public static String translateMiddleLine(String str){
		
		str = str.toLowerCase();
		String newStr = str.replaceAll(" ", WORD_SPACER);
		newStr = newStr.replaceAll("[ghjqrtw]",RAISED+RAISED+LETTER_SPACER);
		newStr = newStr.replaceAll("[bfilpsv]",RAISED+UNRAISED+LETTER_SPACER );
		newStr = newStr.replaceAll("[denoyz]",UNRAISED+RAISED+LETTER_SPACER );
		newStr = newStr.replaceAll("[ackmux]",UNRAISED+UNRAISED+LETTER_SPACER );
		newStr = newStr.trim();
		return newStr;
	}
	
	public static String translateBottomLine(String str){
		
		str = str.toLowerCase();
		String newStr = str.replaceAll(" ", WORD_SPACER);
		newStr = newStr.replaceAll("[uvxyz]",RAISED+RAISED+LETTER_SPACER);
		newStr = newStr.replaceAll("[klmnopqrst]",RAISED+UNRAISED+LETTER_SPACER );
		newStr = newStr.replaceAll("[w]",UNRAISED+RAISED+LETTER_SPACER );
		newStr = newStr.replaceAll("[abcdefghij]",UNRAISED+UNRAISED+LETTER_SPACER );
		newStr = newStr.trim();
		return newStr;
	}
	
	public static String translate(String str){
		String str1 = translateTopLine(str);
		String str2 = translateMiddleLine(str);
		String str3 = translateBottomLine(str);
		str = String.format(str1+"%n"+str2+"%n"+str3+"%n", str1 , str2 , str3);
		return str;
	}

	
}
