package hw2;

import java.util.Scanner;

public class WebUtilities {
	public static void main(String[] args){

	}
	
	public static String getImageLink(String path, int width, int height){

		String str = ("<a href=\""+ path + "\"><img src=\"" + path + "\" width=\"" + width + "\" height=\""+ height +"\"></a>");
		return str;
	}
	
	public static String getHost(String url){
		int begin = url.indexOf("//");
		int end = url.indexOf("/",begin+2);
		url = url.substring(begin+2, end);
		return url;
	}
	
	public static String getTitle(String str){
		int begin = str.indexOf("<title>");
		int end = str.indexOf("</title>",begin+2);
		String str1 = str.substring(begin+7, end);
		return str1;
	}
	
public static String invertHexColor(String color){
		
		int red = Integer.parseInt(color.substring(1, 3),16);
		int green = Integer.parseInt(color.substring(3, 5),16);
		int blue = Integer.parseInt(color.substring(5, 7),16);
		red = 255 - red;
		green = 255 - green;
		blue = 255 - blue;
		color = String.format("#%02x%02x%02x", red, green, blue);
		return color;
	}
	

}
