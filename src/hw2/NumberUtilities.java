package hw2;

import java.util.Scanner;

public class NumberUtilities {
	public static void main(String[] args){
		
	}
	
	public static int round10 (double num){
		double x = Math.abs(num);
		double y = x % 10;
		if(num <= 0){
			if(Math.abs(y) >= 5){
				num = num - (10 - y);
			}
			else{
				num = num + y;
			}
		}
		else{
			if(y >= 5){
				num = num - y + 10;
			}
			else{
				num = num - y;
			}
		}
		return (int)num;
	}
	
	public static int getGameCount(int num){
		num = (int) (Math.pow(2,(num)) -1);
		return num;
	}
	
	public static double getFraction(double num){
		num = Math.abs(num);
		double num1 = (int)(num);
		num = num -num1;
		return num;
	}

}
