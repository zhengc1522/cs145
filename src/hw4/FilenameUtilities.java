package hw4;

import java.io.File;

public class FilenameUtilities {
	public static void main(String[] args) {
		getExtension(new File("honeypot/private/ssn.csv"));
		appendToName(new File("a.docx"), "2");
	}

	public static String getExtension(File file){
		String name = file.getName();
		for(int i=name.length()-1; i>=0; i--){
			
			if(name.charAt(i)=='.'){
				if(i == 0){
					return null;
				}
				return name.substring(i+1);
			}
		}
		return null;
	}

	
	
	
	public static File appendToName (File file, String str){
		String fullName = file.getName();
		int dot= fullName.lastIndexOf('.');
		if(dot == 0){
			fullName = fullName+ "_" + str;
		}
		else if (dot!= -1){
			fullName = fullName.substring(0, dot) + "_" + str + fullName.substring(dot);
		}
		else{
			fullName = fullName+ "_" + str;
		}
		
		return new File(file.getParentFile(), fullName);
	}
}
