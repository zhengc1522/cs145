package hw4;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class ImageUtilities {
	public static void main(String[] args) throws IOException {
		
//	    BufferedImage img = makeSeamless(ImageIO.read(new File("/Users/zhengchuqi/Desktop/flower .jpeg")), 50);
//	    ImageIO.write(img, "jpg", new File("/Users/zhengchuqi/Desktop/cat2.jpg"));
		
	    
	}

	public static BufferedImage swapCorners(BufferedImage image) {
		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);

		int halfWidth = image.getWidth() / 2;
		int halfHeight = image.getHeight() / 2;

		for (int c = 0; c < newImage.getWidth(); ++c) {
			for (int r = 0; r < newImage.getHeight(); ++r) {
				newImage.setRGB(c, r,
						image.getRGB((c + halfWidth) % image.getWidth(), (r + halfHeight) % image.getHeight()));
			}
		}
		return newImage;

	}

	public static BufferedImage getCircleMask(int width, int height, double power) {
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
		double midC = (double) width / 2;
		double midR = (double) height / 2;
		double radius = Math.min(midC, midR);

		for (int r = 0; r < image.getHeight(); r++) {
			for (int c = 0; c < image.getWidth(); c++) {
				double distance = Math.sqrt(Math.pow((c - midC), 2) + Math.pow((r - midR), 2));
				double ndistance = distance / radius;
				double tdistance = Math.pow(ndistance, power);

				if (distance > radius) {
					image.setRGB(c, r, 0);
				} else {
					float gray = (float) Math.abs(1 - tdistance);
					Color color = new Color(gray, gray, gray);
					image.setRGB(c, r, color.getRGB());
				}
			}
		}

		return image;

	}

	public static int mix(int a, int b, double weight) {
		int mixColor = (int) (weight * a + (1 - weight) * b);
		return mixColor;

	}

	public static Color mix(Color a, Color b, double weight) {
		int red = (int) mix(a.getRed(), b.getRed(), weight);
		int green = (int) mix(a.getGreen(), b.getGreen(), weight);
		int blue = (int) mix(a.getBlue(), b.getBlue(), weight);

		Color mixColor = new Color(red, green, blue);

		return mixColor;

	}

	public static BufferedImage addMasked(BufferedImage a, BufferedImage grayscaleMaskA, BufferedImage b,
			BufferedImage grayscaleMaskB) {

		BufferedImage mixImage = new BufferedImage(a.getWidth(), a.getHeight(), BufferedImage.TYPE_INT_RGB);

		for (int c = 0; c < a.getWidth(); c++) {
			for (int r = 0; r < a.getHeight(); r++) {

				Color color1 = new Color(grayscaleMaskA.getRGB(c, r));
				int redMaskA = color1.getRed();
				int greenMaskA = color1.getGreen();
				int blueMaskA = color1.getBlue();

				Color color2 = new Color(grayscaleMaskB.getRGB(c, r));
				int redMaskB = color2.getRed();
				int greenMaskB = color2.getGreen();
				int blueMaskB = color2.getBlue();

				Color color3 = new Color(a.getRGB(c, r));
				int redA = color3.getRed();
				int greenA = color3.getGreen();
				int blueA = color3.getBlue();

				Color color4 = new Color(b.getRGB(c, r));
				int redB = color4.getRed();
				int greenB = color4.getGreen();
				int blueB = color4.getBlue();

				int totalRed = redMaskA + redMaskB;
				int totalGreen = greenMaskA + greenMaskB;
				int totalBlue = blueMaskA + blueMaskB;

				double weightRed = 0.0;
				if (totalRed == 0) {
					weightRed = 0.5;
				} else {
					weightRed = redMaskA / (double) (redMaskA + redMaskB);
				}

				double weightGreen = 0.0;
				if (totalGreen == 0) {
					weightGreen = 0.5;
				} else {
					weightGreen = greenMaskA / (double) (greenMaskA + greenMaskB);
				}

				double weightBlue = 0.0;
				if (totalBlue == 0) {
					weightBlue = 0.5;
				} else {
					weightBlue = blueMaskA / (double) (blueMaskA + blueMaskB);
				}

				Color mix = new Color((int) (weightRed * redA + (1 - weightRed) * redB),
						(int) (weightGreen * greenA + (1 - weightGreen) * greenB),
						(int) (weightBlue * blueA + (1 - weightBlue) * blueB));

				mixImage.setRGB(c, r, mix.getRGB());
			}
		}
		return mixImage;

	}

	public static BufferedImage makeSeamless(BufferedImage image, double power) {
		// BufferedImage newImage1 = swapCorners(image);
		// BufferedImage newImage2 =
		// getCircleMask(newImage1.getWidth(),newImage1.getHeight(), power);
		// getCircleMask(image.getWidth(),image.getHeight(), power);
		// BufferedImage newImage3= swapCorners(newImage2);
		// BufferedImage newImage4 = addMasked(image,newImage2,newImage1,newImage3);
		// BufferedImage newImage4 = addMasked(image,newImage2,newImage1,newImage3);
		// return newImage4;

		BufferedImage newImage1 = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);

		int halfWidth = image.getWidth() / 2;
		int halfHeight = image.getHeight() / 2;

		for (int c = 0; c < newImage1.getWidth(); ++c) {
			for (int r = 0; r < newImage1.getHeight(); ++r) {
				newImage1.setRGB(c, r,
						image.getRGB((c + halfWidth) % image.getWidth(), (r + halfHeight) % image.getHeight()));

			}
		}

		BufferedImage newImage2 = new BufferedImage(image.getWidth(), image.getHeight(),
				BufferedImage.TYPE_BYTE_GRAY);
		double midC = (double) image.getWidth() / 2;
		double midR = (double) image.getHeight() / 2;
		double radius = Math.min(midC, midR);

		for (int r = 0; r < newImage2.getHeight(); r++) {
			for (int c = 0; c < newImage2.getWidth(); c++) {
				double distance = Math.sqrt(Math.pow((c - midC), 2) + Math.pow((r - midR), 2));
				double ndistance = distance / radius;
				double tdistance = Math.pow(ndistance, power);

				if (distance > radius) {
					newImage2.setRGB(c, r, 0);
				} else {
					float gray = (float) Math.abs(1 - tdistance);
					Color color = new Color(gray, gray, gray);
					newImage2.setRGB(c, r, color.getRGB());
				}
			}
		}
		BufferedImage newImage3 = new BufferedImage(newImage2.getWidth(), newImage2.getHeight(), BufferedImage.TYPE_INT_RGB);

		int halfWidth1 = newImage2.getWidth() / 2;
		int halfHeight1 = newImage2.getHeight() / 2;

		for (int c = 0; c < newImage3.getWidth(); ++c) {
			for (int r = 0; r < newImage3.getHeight(); ++r) {
				newImage3.setRGB(c, r,
						newImage2.getRGB((c + halfWidth1) % newImage2.getWidth(), (r + halfHeight1) % newImage2.getHeight()));
			}
		}
		
		BufferedImage newImage4 = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());

		for (int c = 0; c < image.getWidth(); c++) {
			for (int r = 0; r < image.getHeight(); r++) {

				Color color1 = new Color(newImage2.getRGB(c, r));
				int redMaskA = color1.getRed();
				int greenMaskA = color1.getGreen();
				int blueMaskA = color1.getBlue();

				Color color2 = new Color(newImage3.getRGB(c, r));
				int redMaskB = color2.getRed();
				int greenMaskB = color2.getGreen();
				int blueMaskB = color2.getBlue();

				Color color3 = new Color(image.getRGB(c, r));
				int redA = color3.getRed();
				int greenA = color3.getGreen();
				int blueA = color3.getBlue();

				Color color4 = new Color(newImage1.getRGB(c, r));
				int redB = color4.getRed();
				int greenB = color4.getGreen();
				int blueB = color4.getBlue();

				int totalRed = redMaskA + redMaskB;
				int totalGreen = greenMaskA + greenMaskB;
				int totalBlue = blueMaskA + blueMaskB;

				double weightRed = 0.0;
				if (totalRed == 0) {
					weightRed = 0.5;
				} else {
					weightRed = redMaskA / (double) (redMaskA + redMaskB);
				}

				double weightGreen = 0.0;
				if (totalGreen == 0) {
					weightGreen = 0.5;
				} else {
					weightGreen = greenMaskA / (double) (greenMaskA + greenMaskB);
				}

				double weightBlue = 0.0;
				if (totalBlue == 0) {
					weightBlue = 0.5;
				} else {
					weightBlue = blueMaskA / (double) (blueMaskA + blueMaskB);
				}

				Color mix = new Color((int) (weightRed * redA + (1 - weightRed) * redB),
						(int) (weightGreen * greenA + (1 - weightGreen) * greenB),
						(int) (weightBlue * blueA + (1 - weightBlue) * blueB));

				newImage4.setRGB(c, r, mix.getRGB());
			}
		}

		return newImage4;

	}

	public static BufferedImage tile(BufferedImage imageToTile, int horizontalRepetitions, int verticalRepetitions) {
		BufferedImage image = new BufferedImage(imageToTile.getWidth() * horizontalRepetitions,
				imageToTile.getHeight() * verticalRepetitions, imageToTile.getType());

		for (int r = 0; r < imageToTile.getHeight(); ++r) {
			for (int c = 0; c < imageToTile.getWidth(); ++c) {
				for (int vr = 0; vr < verticalRepetitions; ++vr) {
					for (int hr = 0; hr < horizontalRepetitions; ++hr) {
						image.setRGB(c + hr * imageToTile.getWidth(), r + vr * imageToTile.getHeight(),
								imageToTile.getRGB(c, r));
					}
				}

			}
		}

		return image;

	}

}
