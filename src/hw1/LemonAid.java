package hw1;

import java.util.Scanner;

public class LemonAid {

		public static void main(String[] args){
			Scanner in= new Scanner(System.in);
			
				
			System.out.print("How many parts lemon juice? ");
				double nPartOfLemonJuice= in.nextDouble();
			
			System.out.print("How many parts sugar? ");
				double nPartOfSugar= in.nextDouble();
			
			System.out.print("How many parts water? ");
				double nPartOfWater=in.nextDouble();
			
			System.out.print("How many cups of lemonade? ");
				double nCupsOfLemonade=in.nextDouble();
				
			System.out.println("Amounts (in cups):");    
			
			double nLemonJuice = nCupsOfLemonade * nPartOfLemonJuice / (nPartOfLemonJuice + nPartOfSugar + nPartOfWater);	
			System.out.println("  Lemon juice: " + nLemonJuice); 
			
			double nSugar = nCupsOfLemonade * nPartOfSugar / (nPartOfLemonJuice + nPartOfSugar + nPartOfWater);
			System.out.println("  Sugar: " + nSugar);
			
			double nWater = nCupsOfLemonade * nPartOfWater / (nPartOfLemonJuice + nPartOfSugar + nPartOfWater);
			System.out.println("  Water: " + nWater);
		}				

}
