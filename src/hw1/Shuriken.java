package hw1;

import java.util.Scanner;

public class Shuriken {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		
		System.out.print("Spin? ");
		double s = in.nextDouble();
		
		for (int i=0; i<=360; i=i+30){
			double x=(1.1 + Math.sin(Math.toRadians(4 * i))) * Math.cos(Math.toRadians(i + s));
			double y=(1.1 + Math.sin(Math.toRadians(4 * i))) * Math.sin(Math.toRadians(i + s));
		
			System.out.printf("%.2f,%.2f%n", x , y);
			
			
		}
	}
}