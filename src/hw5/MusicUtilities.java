package hw5;

import hw5.speccheck.Accidental;
import hw5.speccheck.Duration;
import hw5.speccheck.Letter;
import hw5.speccheck.Note;
//import hw5.speccheck.WavIO;import hw5.speccheck.Note[];

public class MusicUtilities {
	public static Note[] getScale(Note rootNote, int[] halfStepID) {
		Letter letter = rootNote.getLetter();
		Letter[] letter1 = new Letter[8];
		letter1[0] = Letter.A;
		letter1[1] = Letter.B;
		letter1[2] = Letter.C;
		letter1[3] = Letter.D;
		letter1[4] = Letter.E;
		letter1[5] = Letter.F;
		letter1[6] = Letter.G;
		Note[] newNote = new Note[halfStepID.length];
		int c=0;
		for (int i = 0; i < newNote.length; i++) {
			if (letter1[i] == letter) {
				int num = 7 - i;
				for (int h = 0; h < num; h++) {
					for ( c= 0; c < newNote.length; c++) {

						newNote[h] = new Note(letter1[i + h], rootNote.getAccidental(), rootNote.getOctave(),
								rootNote.getDuration());
						
						num += num+1;
						
					}
				}

			}
		}

		return newNote;

	}

	public static Note[] getMajorScale(Note rootNote) {
		Note[] note = new Note[8];

		Letter letter = rootNote.getLetter();
		Letter[] letter1 = new Letter[8];
		letter1[0] = Letter.A;
		letter1[1] = Letter.B;
		letter1[2] = Letter.C;
		letter1[3] = Letter.D;
		letter1[4] = Letter.E;
		letter1[5] = Letter.F;
		letter1[6] = Letter.G;

		Note[] newNote = new Note[8];

		for (int i = 0; i < 8; i++) {
			if (letter1[i] == letter) {
//				System.out.println(i);
				int num = 7 - i;
				for (int h = 0; h < num; h++) {
					for (int m = num; m < 7; m++) {

						newNote[h] = new Note(letter1[i + h], rootNote.getAccidental(), rootNote.getOctave(),
								rootNote.getDuration());
						newNote[m] = new Note(letter1[m - num], rootNote.getAccidental(), rootNote.getOctave(),
								rootNote.getDuration());
						newNote[7] = new Note(letter, rootNote.getAccidental(), rootNote.getOctave() + 1,
								rootNote.getDuration());

					}
				}

			}
		}

		return newNote;

	}

	public static Note[] getMinorPentatonicBluesScale(Note rootNote) {
		return null;

	}

	public static Note[] getBluesScale(Note rootNote) {
		return null;

	}

	public static Note[] getNaturalMinorScale(Note rootNote) {
		return null;

	}

	public static Note[] join(Note[] first, Note[] second) {
		int length1 = first.length;
		int length2 = second.length;

		Note[] newNotes = new Note[length1 + length2];

		for (int i = 0; i < length1; i++) {
			newNotes[i] = first[i];
		}
		for (int i = 0; i < length2; i++) {
			newNotes[length1 + i] = second[i];
		}

		return newNotes;
	}

	public static Note[] repeat(hw5.speccheck.Note[] notes, int cycle) {
		int length = notes.length;
		Note[] newNote = new Note[length * cycle];

		for (int i = 0; i < newNote.length; i++) {
			newNote[i] = notes[i % length];
		}

		return newNote;
	}
	public static Note[] ramplify(Note[] note, double startAmplitude, double endAmplitude) {
		return null;

	}

	public static Note[] reverse(Note[] notes) {
		Note[] reversed = new Note[notes.length];

		for (int i = 0; i < notes.length; i++) {
			reversed[i] = notes[notes.length - 1 - i];
		}

		return reversed;
	}
	public static Note[] transpose(Note[] note, Note newRoot) {
		return null;

	}

	public static Note[] invert(Note[] note, Note pivotNote) {
		return null;

	}
}
